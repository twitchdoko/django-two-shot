from django.shortcuts import redirect
from django.urls import path
from receipts.views import receipts, create_receipt, expenses, account, create_category, create_account

urlpatterns = [
    path("", receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expenses, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", account, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),

]
