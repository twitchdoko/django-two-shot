from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
# Create your views here.
@login_required
def receipts(request):
    current_user = request.user
    receipts = Receipt.objects.filter(purchaser=current_user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/receipts.html", context)

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        print('user submitted form')
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)

@login_required
def expenses(request):
    current_user = request.user
    expenses = ExpenseCategory.objects.filter(owner=current_user)
    context = {
        "expenses": expenses,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account(request):
    current_user = request.user
    accounts = Account.objects.filter(owner=current_user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/accounts.html", context)



@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form
    }
    return render(request, "categories/create.html", context)

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "accounts/create.html", context)